=======
deployd
=======

Run a Python function remotely using a redis list as transport.

Requires `Python client for redis`__

.. __: https://pypi.python.org/pypi/redis/

Author
======

deployd is developed and maintained by Miguel González. It can be found
in https://bitbucket.org/migonzalvar/deployd

License
=======

BSD 3-Clause License
