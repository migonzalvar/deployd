# -*- coding: utf-8 -*-
"""Run a Python function remotely using a redis list as transport."""

import logging
import subprocess
import os
import sys

from json import dumps, loads
from uuid import uuid4

from redis import StrictRedis


logging.basicConfig()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

QUEUE_KEY = 'deployd'
TTL = 1800

redis = StrictRedis()


# Commands

def _git_pull(path):
    cwd = os.path.abspath(path)
    result = subprocess.check_output(['git', 'pull', 'origin', 'master'], cwd=cwd)
    return result


def command_factory(fn):
    if fn == 'pull':
        return _git_pull
    else:
        raise KeyError


def send(fn, *args, **kwargs):
    key = '%s:%s' % (QUEUE_KEY, uuid4())
    msg = dumps((fn, key, args, kwargs))
    redis.rpush(QUEUE_KEY, msg)
    return key


# Worker

def worker():
    logger.info('Starting up worker')
    try:
        while True:
            msg = redis.blpop(QUEUE_KEY)
            logger.debug('Received %s' % (msg, ))
            fn, key, args, kwargs = loads(msg[1])
            try:
                actual_function = command_factory(fn)
            except KeyError:
                logger.error('%s function does not exist' % (fn, ))
                continue
            try:
                rv = actual_function(*args, **kwargs)
            except Exception, e:
                rv = e
                logger.warning(e)
            else:
                logger.debug('Return %s' % (repr(rv), ))
            if rv is not None:
                redis.set(key, dumps(rv))
                redis.expire(key, TTL)
    except KeyboardInterrupt:
        logger.info('Shutting down worker')
        sys.exit(0)


# Console entry points

def server_cli():
    worker()


def run_cli_usage():
    return """Usage:
    %s <function> [arg1] [arg2]...
""" % sys.argv[0]


def run_cli():
    if len(sys.argv) < 2:
        print('ERROR: <function> name is mandatory.')
        print(run_cli_usage())
        sys.exit(2)
    fn = sys.argv[1]
    args = sys.argv[2:]
    key = send(fn, *args)
    print(key)
