from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read()

setup(
    name='deployd',
    version='0.1',
    packages=['deployd'],
    entry_points={
        'console_scripts': [
            'deployd-server = deployd:server_cli',
            'deployd-run = deployd:run_cli',
        ]
    },
    install_requires=requirements,
    url='https://bitbucket.org/migonzalvar/deployd',
    license='',
    author='migonzalvar',
    author_email='migonzalvar@gmail.com',
    description='Run a Python function remotely using a redis list as transport.',
    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
    ],
)
